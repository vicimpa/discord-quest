import { botToken, channels } from "./config";
import { Bot } from "lib/Bot";
import { Quests } from "lib/Quests";

const bot = new Bot(botToken)

async function main() {
  await bot.awaitReady()

  console.log('Ready...')

  for(let ch of channels) {
    try {
      let game = new Quests(bot, ch)
      game.run().catch(console.error)
    }catch(e) {
      console.error(e)
    }
  }
}

main().catch(console.error)