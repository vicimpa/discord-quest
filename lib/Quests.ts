import { EventEmitter } from "events";
import { Bot } from "lib/Bot";
import { User, Channel, Message } from "discord.js";
import { rand } from "lib/Utilites";
import { Cols } from "lib/Cols";
import { promises as fs } from "fs";

export interface IWord {
  word: string
  author: User
}

const score = new Cols('очк', 'о', 'а', 'ов')
const chars = new Cols('букв', 'а', 'ы', '')
const nums = new Cols('цыфр', 'а', 'ы', '')
const errors = new Cols('ошиб', 'ка', 'ки', 'ок')

interface IStoreData {
  [key: string]: {
    username?: string
    hash?: string
    score: number
    all: number
    success: number
  }
}

async function getQuest() {
  let quests = (<string>await fs.readFile('./quests', 'utf-8')).split('\n')

  quests = quests.sort(() => Math.random() > 0.5 ? 1 : -1)

  for (let questText of quests) {
    let [quest, answer] = questText.split('*')

    if (/^[a-zа-я0-9]+$/i.test(answer))
      return [quest, answer]
  }
}

export class Quests extends EventEmitter {
  public work = false

  private quest: string = ''
  private word: string[] = []
  private oWord: number[] = []

  private isQuest = false

  private listen = true

  private timeWarn: NodeJS.Timer
  private timeOut: NodeJS.Timer

  private interval: NodeJS.Timer = null
  private randNum: number = 1

  private get score() {
    return Math.floor(this.getWordMap().length * (20 + this.randNum))
  }

  private async readStore(): Promise<IStoreData> {
    try {
      return JSON.parse(<string>await fs.readFile(`./data/storage-${this.channel}.json`, 'utf-8'))
    } catch (e) {
      return {}
    }
  }

  private async saveStore(data: IStoreData) {
    await fs.writeFile(`./data/storage-${this.channel}.json`, JSON.stringify(data, null, 2), 'utf-8')
  }

  constructor(public bot: Bot, public channel: string) {
    super()

    this.on('start', () => {
      if (this.work)
        return null

      this.work = true

      this.sayMessage('Игра началась', 0x0000FF)
        .catch(console.error)

      this.newWord()

      this.on('win', (author: User) => {
        this.sayMessage(`Ответ "${this.word.join('')}"\nПобедил(а) <@${author.id}> и получил ${score.format(this.score)}`, 0x00FFFF)
        this.clearInterval()

        this.isQuest = false

        setTimeout(() => {
          if (this.work && !this.isQuest)
            this.newWord()
        }, 5000)
      })

      this.on('fail', () => {
        this.sayMessage(`Ответ был "${this.word.join('')}"\nНикто не разгадал его`, 0xFF0000)
          .catch(console.error)

        this.isQuest = false

        if (this.work && !this.isQuest)
          this.newWord()
      })
    })

    this.on('stop', () => {
      if (!this.work)
        return null

      this.work = false
      this.isQuest = false

      this.sayMessage('Игра остановлена', 0x0000FF)
        .catch(console.error)

      this.sayHelp()

      this.clearInterval()

      this.removeAllListeners('fail')
      this.removeAllListeners('win')
      this.removeAllListeners('skip')
    })
  }

  public async newWord() {
    let [quest, answer] = await getQuest()

    this.isQuest = true
    this.clearInterval()
    this.randNum = rand(100, 1000) * 0.01
    this.setWord(quest, answer)

    this.setInterval(() => {
      let { cols } = this
      this.randOpen() &&
        this.sayWord(`Я открыл одну ${cols.getCol(0)}у`)
    }, 15000)
  }

  public async destroy() {
    this.listen = false
    this.bot.emit(`message-${this.channel}`, { content: '!fake' })
    this.emit('stop')
    this.removeAllListeners()
  }

  public async sayTop() {
    let store = await this.readStore()
    let list = Object.keys(store).map(
      e => { return { id: e, score: store[e] } }).sort(
        (a, b) => a.score.score > b.score.score ? -1 : a.score.score < b.score.score ? 1 : 0)

    let textChanks: string[] = []

    let topKpd: string = ''
    let topKpdScore: number = -1

    let topActive: string = ''
    let topActiveScore: number = -1

    for (let i = 0; i < 10; i++) {
      let scoreObject = list[i]

      if (!scoreObject)
        continue;

      let { id, score: { score: count, success, all, username = null } } = scoreObject
      let kpd = Math.floor(success / all * 100)

      if (kpd > topKpdScore) {
        topKpdScore = kpd
        topKpd = id
      }

      if (all > topActiveScore) {
        topActiveScore = all
        topActive = id
      }

      textChanks.push(`${i + 1} - <@${id}> - ${score.format(count)} - ${kpd}% (${success}/${all}) КПД`)
    }

    let topActiveUser = store[topActive]
    let topKpdUser = store[topKpd]

    textChanks.push('\n')

    if(topKpdUser)
    {
      let { success, all, username } = topKpdUser
      textChanks.push(`- - <@${topKpd}> Самый точный игрок - ${topKpdScore}% (${success}/${all}) КПД `)
    }

    if(topActiveUser)
    {
      let { success, all, username } = topActiveUser
      textChanks.push(`- - <@${topActive}> Самый активный игрок - (${success}/${all}) попыток ответить`)
    }


    this.sayMessage(`Топ 10 игроков в викторину:\n${textChanks.join('\n')}`, 0x0000FF)
  }

  public async sayScore(userId: string) {
    let store = await this.readStore()
    let user = store[userId]
    let { success, all, score: count } = user || { score: 0, all: 0, success: 0 }

    this.sayMessage(`<@${userId}> имеет сейчас ${score.format(count)} - ${Math.floor(((success / all) || 0) * 100)}% КПД`, 0x0000FF)
  }

  public async run() {
    let { channel } = this

    while (this.listen) {
      let { cmd, message } = await this.bot.awaitCommand(channel)


      message.client.user.setPresence({
        status: 'online',
        game: {
          name: 'Викторина',
          type: 'PLAYING',
          url: 'https://vk.com/vicimpa'
        }
      })

      let autodelete = true, { author, channel: channelObject } = message
      let timeForWarn = 1.5 * 60 * 1000
      let timeForOut = timeForWarn + 30000

      if (this.timeWarn) {
        clearTimeout(this.timeWarn)
        this.timeWarn = null
      }

      if (this.timeOut) {
        clearTimeout(this.timeOut)
        this.timeOut = null
      }

      switch (cmd) {
        case 'start':
          this.emit('start')
          break;

        case 'stop':
          this.emit('stop')
          break;

        case 'top':
          this.sayTop()
          break;

        case 'score':
          this.sayScore(author.id)
          break;

        case 'help':
          this.sayHelp()
          break;

        case 'fskip':
          if (!this.word)
            break;

          this.sayMessage(`<@${author.id}> пропустил вопрос`, 0xF88070)
            .catch(console.error)

          this.newWord()
          break;

        default:
          autodelete = false
          this.sendWord(author, cmd)
      }

      if (this.work) {
        this.timeWarn = setTimeout(() => {
          this.sayMessage('Игра будет остановлена через 30 сек из-за неактивности!', 0xF88070)
            .catch(console.error)
        }, timeForWarn)

        this.timeOut = setTimeout(() => {
          this.emit('stop')
        }, timeForOut)
      }

      autodelete && message.deletable &&
        message.delete().catch(console.error)
    }
  }

  public get cols() {
    return /^[0-9]+$/.test(this.word.join('')) ? nums : chars
  }

  public async sendWord(author: User, word: string) {
    if (!this.isQuest)
      return null

    let data = await this.readStore()
    let user = data[author.id] || (data[author.id] = { score: 0, all: 0, success: 0 })

    user.username = author.username
    user.hash = author.tag
    word = word.toLowerCase().trim()

    user.all++

    if (word === this.word.join('')) {
      this.emit('win', author, word)
      this.isQuest = false
      user.success++
      user.score += this.score
    }

    await this.saveStore(data)
  }

  public async sayHelp() {
    let text = '', addRow = (row: string = '') =>
      text += row + '\n'

    addRow('Команды для викторины:')
    addRow()
    addRow('!start - Запуск игры')
    addRow('!stop - Остановка игры')
    addRow()
    addRow('!top - Топ 10 игроков в викторину')
    addRow('!score - Получить количество очков')
    addRow('!fskip - Пропустить текущий вопрос')
    addRow()
    addRow('!help - Получить помощь')
    this.sayMessage(text, 0x0000FF, 'Флудить при помощи бота не стоит. Будем карать!')
  }

  public getDescriptionWord() {
    let outWord = this.word.map(
      (e, i) => this.oWord[i] ? e : '_')

    return outWord.join(' ')
  }

  public setInterval(callback: () => void, n: number) {
    this.clearInterval()
    this.interval = setInterval(callback, n)
  }

  public clearInterval() {
    if (this.interval)
      clearInterval(this.interval)

    this.interval = null
  }

  public setWord(quest: string, answer: string) {
    this.quest = quest
    this.word = answer.toLowerCase().trim().split('')
    this.oWord = this.word.map(() => 0)
    this.sayWord('Новый вопрос!')
    this.isQuest = true
  }

  public getWordMap() {
    return this.oWord
      .map((e, i) => e ? -1 : i)
      .filter(e => e !== -1)
  }

  public randOpen() {
    let wordMap = this.getWordMap()

    if (wordMap.length == 1) {
      this.emit('fail')
      return null
    }

    let randIndexOpen = wordMap[rand(0, wordMap.length - 1)]

    this.oWord[randIndexOpen] = 1
    return true
  }

  public sayWord(title?: string) {
    let text = '', addRow =
      (row: string) => text += row.trim() + '\n'

    let { word, cols } = this

    title && addRow(`${title} - (${score.format(this.score)})`)
    addRow(`${this.quest}`)
    addRow(`(${this.getDescriptionWord()}) ${cols.format(word.length)}`)

    this.sayMessage('`' + text + '`', 0x00ff00, 'Пример ответа: !ответ')
      .catch(console.error)
  }

  public async sayMessage(text: string, color: number, footer?: string) {
    return await this.bot.sendText(this.channel, text, { color, footer })
  }
}