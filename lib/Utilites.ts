export function rand(min: number, max: number) {
  var rand = min - 0.5 + Math.random() * (max - min + 1)
  rand = Math.round(rand);
  return rand;
}

export function delay(n = 0) {
  return new Promise<void>(r => setTimeout(r, n))
}