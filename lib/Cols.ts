export class Cols{
  base: string
  one: string
  two: string
  all: string

  constructor(base: string, one: string, two: string, all: string) {
    this.base = base
    this.one = one
    this.two = two
    this.all = all
  }

  format(n: number, temp: string = '%n %c') {
    return temp.replace(/%n/g, n.toString())
      .replace(/%c/g, this.getCol(n))
  }

  getCol(n: number) {
    let arr = n.toString().split('')
    let a = arr.pop()
    let b = arr.pop()

    let {base, one, two, all} = this

    if(b === '1')
      a = '5'

    switch(a) {
      case '1': 
        return `${base}${one}`;
      case '2':
      case '3':
      case '4':
        return `${base}${two}`
      default:
        return `${base}${all}`
    }
  }
}