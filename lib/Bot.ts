import { Client, Message, User, RichEmbed, TextChannel, Channel } from "discord.js";
import { EventEmitter } from "events";
import { delay } from "lib/Utilites";

export interface IMessageOptions {
  color?: number
  title?: string
  reply?: User
  footer?: string
}

export class Bot extends EventEmitter {
  public client: Client = new Client()
  public connected = false
  public textChannel: TextChannel = null

  public get username() {
    while (!this.client) { }
    return this.client.user.username
  }

  constructor(private token: string) {
    super()
    let { client } = this

    client.on('message', this._onMessage.bind(this))
    client.on('messageUpdate', this._onMessageUpdate.bind(this))


    client.on('ready', () => this.connected = true)

    client.login(token)
    this.setMaxListeners(0)
  }

  private _onMessage(mess: Message) {
    let { channel } = mess
    this.emit(`message-${channel.id}`, mess)
  }

  private _onMessageUpdate(oldMess: Message, newMess: Message) {
    let { channel } = oldMess
    this.emit(`messageUpdate-${channel.id}`, newMess)
  }

  public onMessage(channel: string, listener: (mess: Message) => void) {
    this.on(`message-${channel}`, listener)
  }

  public offMessage(channel: string, listener?: (mess: Message) => void) {
    this.off(`message-${channel}`, listener)
  }

  public async awaitMessage(channel: string, regExp?: RegExp): Promise<Message> {
    return new Promise<Message>(resolve => {
      let onMessage = (mess: Message) => {
        if (regExp && !regExp.test(mess.content))
          return null

        resolve(mess)
        this.offMessage(channel, onMessage)
      }

      this.onMessage(channel, onMessage)
    })
  }

  public async awaitCommand(channel: string) {
    let reg = /^\!([a-z0-9а-я]+)/i
    let message = await this.awaitMessage(channel, reg)

    return { cmd: reg.exec(message.content)[1], message }
  }

  public async awaitReady(): Promise<void> {
    if (this.client)
      return null

    return new Promise<void>(resolve => {
      this.client.once('ready', () => {
        while(!this.client.user){}
        resolve()
      })
    })
  }

  public async getTextChannel(channel: string, n = 0): Promise<TextChannel> {
    let channelObject = this.client.channels.get(channel)
    let attempts = 200
  
    if(!channelObject && n < attempts) {
      await delay(200)
      return await this.getTextChannel(channel, ++n)
    }
  
    if(!channelObject && n >= attempts)
      throw new Error('No find Room!')

    if(!(channelObject instanceof TextChannel))
      throw new Error('Find room is not TextChannel')
  
    return <TextChannel><any>channelObject
  }

  public async sendText(channel: string | Channel, text: string, data: IMessageOptions = {}) {
    let channelObject: TextChannel = null

    if(typeof channel === 'string')
      channelObject = await this.getTextChannel(channel)

    if(channel instanceof TextChannel)
      channelObject = channel

    let { reply, color, title, footer } = data
    let rich = new RichEmbed({ description: text, title, color, footer: {text: footer} })

    return await channelObject.send('', { embed: rich, reply })
  }
}